#!/bin/bash

export DOCKER_CLIENT_TIMEOUT=120
export COMPOSE_HTTP_TIMEOUT=120

RUNNER_HOME=/home/briecarranza/gitlab-docker/runner GITLAB_HOME=/home/briecarranza/gitlab-docker/gitlab/ docker-compose up -d
